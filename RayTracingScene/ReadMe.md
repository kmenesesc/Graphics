To Run stand-alone application, find the .exe file under the RayTrace2 folder.

The following files compile a scene with a glass object in the middle of the scene, a pink ball in the front and two other “ring toys” on the sides. The background wall is a mirror and the two side walls are tinted mirrors. The scene incorporates jittered stochastic sampling to help with aliasing and motion blur on the pink ball. One can also observe transmission and reflection to a trace depth of six.

The following is an image with a supersampling for motion blur and aliasing at a 6x6 pixel grid.

<img src="https://i.postimg.cc/P5s4G2mj/Motionand-Sampling.png">

The following does not incorporate distributed ray tracing.

<img src="https://i.postimg.cc/BQ1gwdPp/No-Distributive.png">
