The program draws a cat on a rice bowl. The image is made up of 10 bezier patches. Two for the face, two for the upper part of the bowl, two for the stand of the bowl and four for the ears. The program also uses three different texture maps which are the following: One for the bowl, one for the face of the cat and one for the rest of the cat’s body. The body of the cat-bun has different material properties than the bowl to stress the shininess of the bowl versus the shininess of the cat. 

Copy and paste backBun.bmp, bun.bmp and cup.bmp from the bun directory into the Debug directory to run bun.exe as a stand-alone executable.

To control the mesh of the entire rendering:
*   Press "U" to increase the number samples in U direction.
*   Press "u" to decrease the number samples in U direction.
*   Press "V" to increase the number samples in V direction.
*   Press "v" to decrease the number samples in V direction.
To control the animation:
*   Press the "a" key to toggle the animation off and on.
*   Press the "s" key to perform a single step of the animation.
*   The left and right arrow keys controls the	rate of rotation around the y-axis.
*   The up and down arrow keys increase and decrease the rate of		rotation around the x-axis. 
*   In order to reverse rotational direction you must zero or reset the patch ("0" or "r").
*   Press the "r" key to reset back to initial	position, with no rotation.
*   Press "0" (zero) to zero the rotation rates.

 Here’s a picture of the program: 
 
<div class="inline-block">
 <img src="https://i.postimg.cc/Kzq1NH8y/bun2.png" width="250" height="250">
 <img src="https://i.postimg.cc/nrNC89mR/bun1.png" width="250" height="250">
</div>

And here’s a picture of the inspiration (All credit to the show: “Is the order a rabbit?” for the image): 

<img src="https://i.postimg.cc/j2DLk4Dn/bun3.png">

